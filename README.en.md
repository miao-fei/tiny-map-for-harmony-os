# TinyMap For HarmonyOS

#### Description
Easy-to-use lightweight map component for HarmonyOS

Advantages:
1. Super lightweight, currently only contains three classes, suitable for source code research and learning.
2. Gaude map and Google map can be used as geographic base map, and satellite image and vector base map data can be selected.
3. Add custom base map elements (elements)

#### Installation

Move the tinymap module (HAP) to the required project, and import the project into the module (HAP) to be called.
Insert the following code in build.gradle:

```
apply plugin: 'com.huawei.ohos.hap'
...

dependencies {
    ...
    implementation project(':tinymap')
}

```

#### Instructions

At present, it supports the sliding and translation of gestures, but does not support the zooming in and zooming out of the gestures (this function will be added later).

1. zoomIn() method: zoom in the map
2. zoomOut() method: zoomout the map
2. RefreshMap() method: refresh the map
3. setMapSource(TinyMap.MapSource MapSource) method: switch the base map data source. At present, there are five types of base map data sources

> MapSource.GAODE_ROAD : Gaode Road data
> MapSource.GAODE_VECTOR : Gaode vector data
> MapSource.GAODE_SATELLITE : Gaode satellite data
> MapSource.GOOGLE_VECTOR : Google vector data
> MapSource.GOOGLE_SATELLITE : Google satellite data

5. Addelement (float x, float y, int resource) method: add base map elements (at present, only Mercator projection coordinates are supported, and latitude and longitude coordinate methods will be added later). For example:

`addElement(12956517.35f, 4864667.87f, ResourceTable.Media_ dot)`

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
