package com.dongyu.tinymap.slice;

import com.dongyu.tinymap.ResourceTable;
import com.dongyu.tinymap.TinyMap;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.render.opengl.Utils;
import ohos.agp.window.dialog.IDialog;
import ohos.agp.window.dialog.ListDialog;
import ohos.hiviewdfx.HiLog;

public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        // obtain the map object
        TinyMap tinyMap = (TinyMap) findComponentById(ResourceTable.Id_map);

        // zoom in
        Button btnZoomIn = (Button) findComponentById(ResourceTable.Id_btn_zoomin);
        btnZoomIn.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                tinyMap.zoomIn();
            }
        });

        // zoom out
        Button btnZoomOut = (Button) findComponentById(ResourceTable.Id_btn_zoomout);
        btnZoomOut.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                tinyMap.zoomOut();
            }
        });

        // change base map
        Button btnChangeBaseMapType = (Button) findComponentById(ResourceTable.Id_btn_changebasemaptype);
        btnChangeBaseMapType.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                ListDialog listDialog = new ListDialog(MainAbilitySlice.this, ListDialog.SINGLE);
                listDialog.setItems(new String[]{"高德地图 - 道路",
                        "高德地图 - 矢量",
                        "高德地图 - 栅格"});
                listDialog.setOnSingleSelectListener(new IDialog.ClickedListener() {
                    @Override
                    public void onClick(IDialog iDialog, int i) {
                        if (i == 0)
                            tinyMap.setMapSource(TinyMap.MapSource.GAODE_ROAD);
                        if (i == 1)
                            tinyMap.setMapSource(TinyMap.MapSource.GAODE_VECTOR);
                        if (i == 2)
                            tinyMap.setMapSource(TinyMap.MapSource.GAODE_SATELLITE);
                        tinyMap.refreshMap();
                        listDialog.hide();
                    }
                });
                listDialog.setButton(0, "取消", new IDialog.ClickedListener() {
                    @Override
                    public void onClick(IDialog iDialog, int i) {
                        listDialog.hide();
                    }
                });
                listDialog.setSize(600, 600);
                listDialog.show();
            }
        });

        // add element on the map
        Button btnAddElement = (Button) findComponentById(ResourceTable.Id_btn_addElement);
        btnAddElement.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                double[] coord = toMercator(116.390372, 39.991230);
                tinyMap.addElement((float) coord[0], (float)coord[1], ResourceTable.Media_dot);
            }
        });
    }
    /**
     * 将经纬度转换为墨卡托投影
     * @param lon 经度
     * @param lat 纬度
     * @return 墨卡托投影
     */
    public static double[] toMercator(double lon,double lat)
    {
        double[] xy = new double[2];
        double x = lon * 20037508.342789/180;
        double y = Math.log(Math.tan((90 + lat) * Math.PI / 360))
                / (Math.PI / 180);
        y = y * 20037508.34789 / 180;
        xy[0] = x;
        xy[1] = y;
        return xy;
    }

    /**
     * 将墨卡托坐标转换为经纬度
     * @param mercatorX 墨卡托投影X坐标
     * @param mercatorY 墨卡托投影Y坐标
     * @return 经纬度坐标
     */
    public static double[] toLonLat(double mercatorX,double mercatorY)
    {
        double[] xy = new double[2];
        double x = mercatorX / 20037508.34 * 180;
        double y = mercatorY / 20037508.34 * 180;
        y= 180 / Math.PI * (2 * Math.atan( Math.exp
                ( y * Math.PI / 180)) - Math.PI / 2);
        xy[0] = x;
        xy[1] = y;
        return xy;
    }
}
